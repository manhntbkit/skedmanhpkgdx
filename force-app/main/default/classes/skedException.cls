public class skedException extends System.Exception {

	/** Related records to the exception being thrown. */
	public sObject[] relatedObjects;	

	/** Option code to associate error with in addition to base exception message */
	public String code;

	public skedException(String message, String code) {
		this.setMessage(message);
		this.code = code;
	}

	public skedException(String message, sObject[] relatedObjects) {
		this.setMessage(message);
		this.relatedObjects = relatedObjects;
	}

	public skedException(String message, String code, sObject[] relatedObjects) {
		this.setMessage(message);
		this.code = code;
		this.relatedObjects = relatedObjects;
	}

	/**
	 * @description Formats the message, code, and related objects.
	 */
	public virtual String toString() {
		// todo: loop through related objects and get Id and name of object		
		return this.code + '::' + this.getMessage() + '::' + this.getStackTraceString();
	}

	
}